/**
 * \file
 *
 * \brief Main functions for Keyboard example
 *
 * Copyright (c) 2009-2015 Atmel Corporation. All rights reserved.
 *
 * \asf_license_start
 *
 * \page License
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. The name of Atmel may not be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * 4. This software may only be redistributed and used in connection with an
 *    Atmel microcontroller product.
 *
 * THIS SOFTWARE IS PROVIDED BY ATMEL "AS IS" AND ANY EXPRESS OR IMPLIED
 * WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT ARE
 * EXPRESSLY AND SPECIFICALLY DISCLAIMED. IN NO EVENT SHALL ATMEL BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
 * ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * \asf_license_stop
 *
 */
/*
 * Support and FAQ: visit <a href="http://www.atmel.com/design-support/">Atmel Support</a>
 */

#include <asf.h>
#include "conf_usb.h"
#include "ui.h"

//@OURSELVES
//entrada1
#define in1_id ID_PIOD
#define in1_pio PIOD
#define in1_pin  22
#define in1_mask (1 << in1_pin)
//entrada2
#define in2_id ID_PIOD
#define in2_pio PIOD
#define in2_pin  21
#define in2_mask (1 << in2_pin)
//entrada3
#define in3_id ID_PIOB
#define in3_pio PIOB
#define in3_pin  1
#define in3_mask (1 << in3_pin)
//entrada4
#define in4_id ID_PIOA
#define in4_pio PIOA
#define in4_pin  4
#define in4_mask (1 << in4_pin)
//entrada5
#define in5_id ID_PIOC
#define in5_pio PIOC
#define in5_pin  17
#define in5_mask (1 << in5_pin)
//entrada6
#define in6_id ID_PIOC
#define in6_pio PIOC
#define in6_pin  30
#define in6_mask (1 << in6_pin)
//saida1
#define out1_id ID_PIOA
#define out1_pio PIOA
#define out1_pin  19
#define out1_mask (1 << out1_pin)
//saida2
#define out2_id ID_PIOC
#define out2_pio PIOC
#define out2_pin  31
#define out2_mask (1 << out2_pin)
//saida3
#define out3_id ID_PIOB
#define out3_pio PIOB
#define out3_pin  3
#define out3_mask (1 << out3_pin)
//saida4
#define out4_id ID_PIOA
#define out4_pio PIOA
#define out4_pin  0
#define out4_mask (1 << out4_pin)
//saida5
#define out5_id ID_PIOD
#define out5_pio PIOD
#define out5_pin  28
#define out5_mask (1 << out5_pin)
//saida6
#define out6_id ID_PIOA
#define out6_pio PIOA
#define out6_pin  3
#define out6_mask (1 << out6_pin)
//saida7
#define out7_id ID_PIOB
#define out7_pio PIOB
#define out7_pin  0
#define out7_mask (1 << out7_pin)
//saida8
#define out8_id ID_PIOD
#define out8_pio PIOD
#define out8_pin  25
#define out8_mask (1 << out8_pin)
//saida9
#define out9_id ID_PIOD
#define out9_pio PIOD
#define out9_pin  25
#define out9_mask (1 << out9_pin)

static volatile bool main_b_kbd_enable = false;
//Dicionario de teclas

const uint8_t a = HID_A;
const uint8_t w = HID_W;
const uint8_t s = HID_S;
const uint8_t e = HID_E;
const uint8_t d = HID_D;
const uint8_t f = HID_F;
const uint8_t t = HID_T;
const uint8_t g = HID_G;
const uint8_t y = HID_Y;
const uint8_t h = HID_H;
const uint8_t u = HID_U;
const uint8_t j = HID_J;
const uint8_t k = HID_K;
const uint8_t o = HID_O;
const uint8_t l = HID_L;
const uint8_t p = HID_P;
const uint8_t C = HID_COMMA; //COMMA
const uint8_t S = HID_COLON; //SEMICOLON


uint8_t keys[49];

void initKeys(){
	uint8_t tempKeys[49] = {
		//  1 2 3 4 5 6 7 8 9
		t,k,0,0,0,0,0,0,0, //1
		f,j,0,S,0,0,0,0,0, //2
		d,u,0,C,0,0,0,0,0, //3
		e,h,0,p,0,0,0,0,0, //4
		s,y,0,l,0,0,0,0,0, //5
		w,g,0,o,0,0,0,0,a, //6
	};
	for(int i =0; i < 49; i+=1) {
		keys[i] = tempKeys[i];
	}
}



uint8_t getKeyByCoordenate(uint8_t i, uint8_t j){
	return (keys[((i -1) * 9) + (j -1)]);
}


void checkWichKeyPress(uint8_t i, uint8_t j) {
	udi_hid_kbd_down(getKeyByCoordenate(i,j));
}

void unPress(uint8_t i, uint8_t j) {

	udi_hid_kbd_up(getKeyByCoordenate(i,j));
}

uint8_t getColumnIndexActive(void) {
	uint8_t index = 0;
	if(pio_get_output_data_status(out1_pio,out1_mask)) {
		index =1;
	}
	else if(pio_get_output_data_status(out2_pio,out2_mask)) {
		index =2;
	}
	else if(pio_get_output_data_status(out3_pio,out3_mask)) {
		index =3;
	}
	else if(pio_get_output_data_status(out4_pio,out4_mask)) {
		index =4;
	}
	else if(pio_get_output_data_status(out5_pio,out5_mask)) {
		index =5;
	}
	else if(pio_get_output_data_status(out6_pio,out6_mask)) {
		index =6;
	}
	else if(pio_get_output_data_status(out7_pio,out7_mask)) {
		index =7;
	}
	else if(pio_get_output_data_status(out8_pio,out8_mask)) {
		index =8;
	}
	else if (pio_get_output_data_status(out9_pio,out9_mask)) {
		index =9;
	}
	return index;
}



/*! \brief Main function. Execution starts here.
 */
int main(void)
{
	irq_initialize_vectors();
	cpu_irq_enable();

	// Initialize the sleep manager
	sleepmgr_init();
	//@OURSELVES
	initKeys();

	//OURSELVESELEND

	sysclk_init();
	board_init();
	ui_init();
	ui_powerdown();

	// Start USB stack to authorize VBus monitoring
	udc_start();

	// The main loop manages only the power mode
	// because the USB management is done by interrupt
	while (true) {
#ifdef   USB_DEVICE_LOW_SPEED
		// No USB "Keep a live" interrupt available in low speed
		// to scan keyboard interface then use main loop
		if (main_b_kbd_enable) {
			static volatile uint16_t virtual_sof_sub = 0;
			if (sysclk_get_cpu_hz()/50000 ==
				virtual_sof_sub++) {
				virtual_sof_sub = 0;
				static uint16_t virtual_sof = 0;
				ui_process(virtual_sof++);
			}
		}
#else
		sleepmgr_enter_sleep();
#endif
	}
}

void main_suspend_action(void)
{
	ui_powerdown();
}

void main_resume_action(void)
{
	ui_wakeup();
}

void main_sof_action(void)
{
	if (!main_b_kbd_enable)
		return;
	ui_process(udd_get_frame_number());
}

void main_remotewakeup_enable(void)
{
	ui_wakeup_enable();
}

void main_remotewakeup_disable(void)
{
	ui_wakeup_disable();
}

bool main_kbd_enable(void)
{
	main_b_kbd_enable = true;
	return true;
}

void main_kbd_disable(void)
{
	main_b_kbd_enable = false;
}

#include "ui.h"

//! Sequence process running each \c SEQUENCE_PERIOD ms
#define SEQUENCE_PERIOD 150

static struct {
	bool b_modifier;
	bool b_down;
	uint8_t value;
} ui_sequence[] = {
	// Display windows menu
	{true,true,HID_MODIFIER_LEFT_UI},
	// Launch Windows Command line
	{false,true,HID_R},
	{false,false,HID_R},
	// Clear modifier
	{true,false,HID_MODIFIER_LEFT_UI},
	// Tape sequence "notepad" + return
	{false,true,HID_N},
	{false,false,HID_N},
	{false,true,HID_O},
	{false,false,HID_O},
	{false,true,HID_T},
	{false,false,HID_T},
	{false,true,HID_E},
	{false,false,HID_E},
	{false,true,HID_P},
	{false,false,HID_P},
	{false,true,HID_A},
	{false,false,HID_A},
	{false,true,HID_D},
	{false,false,HID_D},
	{false,true,HID_ENTER},
	{false,false,HID_ENTER},
	// Delay to wait "notepad" focus
	{false,false,0}, // No key (= SEQUENCE_PERIOD delay)
	{false,false,0}, // No key (= SEQUENCE_PERIOD delay)
	{false,false,0}, // No key (= SEQUENCE_PERIOD delay)
	{false,false,0}, // No key (= SEQUENCE_PERIOD delay)
	{false,false,0}, // No key (= SEQUENCE_PERIOD delay)
	{false,false,0}, // No key (= SEQUENCE_PERIOD delay)
	{false,false,0}, // No key (= SEQUENCE_PERIOD delay)
	// Display "Atmel "
	{true,true,HID_MODIFIER_RIGHT_SHIFT}, // Enable Maj
	{false,true,HID_A},
	{false,false,HID_A},
	{true,false,HID_MODIFIER_RIGHT_SHIFT}, // Disable Maj
	{false,true,HID_T},
	{false,false,HID_T},
	{false,true,HID_M},
	{false,false,HID_M},
	{false,true,HID_E},
	{false,false,HID_E},
	{false,true,HID_L},
	{false,false,HID_L},
	{false,true,HID_SPACEBAR},
	{false,false,HID_SPACEBAR},
	// Display "ARM "
	{false,true,HID_CAPS_LOCK}, // Enable caps lock
	{false,false,HID_CAPS_LOCK},
	{false,true,HID_A},
	{false,false,HID_A},
	{false,true,HID_R},
	{false,false,HID_R},
	{false,true,HID_M},
	{false,false,HID_M},
	{false,true,HID_CAPS_LOCK}, // Disable caps lock
	{false,false,HID_CAPS_LOCK},
};

// Wakeup pin is SW0 (fast wakeup 14)
#define  WAKEUP_PMC_FSTT (PMC_FSMR_FSTT14)
#define  WAKEUP_PIN      (GPIO_PUSH_BUTTON_1)
#define  WAKEUP_PIO      (PIN_PUSHBUTTON_1_PIO)
#define  WAKEUP_PIO_ID   (PIN_PUSHBUTTON_1_ID)
#define  WAKEUP_PIO_MASK (PIN_PUSHBUTTON_1_MASK)
#define  WAKEUP_PIO_ATTR (PIN_PUSHBUTTON_1_ATTR)

// Interrupt on "pin change" from SW0 to do wakeup on USB
// Note:
// This interrupt is enable when the USB host enable remotewakeup feature
// This interrupt wakeup the CPU if this one is in idle mode
static void ui_wakeup_handler(uint32_t id, uint32_t mask)
{
	if (WAKEUP_PIO_ID == id && WAKEUP_PIO_MASK == mask) {
		// It is a wakeup then send wakeup USB
		udc_remotewakeup();
	}
}

void ui_init(void)
{
	// Enable PIO clock for button inputs
	pmc_enable_periph_clk(ID_PIOB);
	pmc_enable_periph_clk(ID_PIOE);
	// Set handler for wakeup
	pio_handler_set(WAKEUP_PIO, WAKEUP_PIO_ID, WAKEUP_PIO_MASK, WAKEUP_PIO_ATTR, ui_wakeup_handler);
	// Enable IRQ for button (PIOB)
	NVIC_EnableIRQ((IRQn_Type) WAKEUP_PIO_ID);
	// Initialize LEDs
	LED_On(LED0);
}

void ui_powerdown(void)
{
	LED_Off(LED0);
}


void ui_wakeup_enable(void)
{
	// Enable interrupt for button pin
	pio_configure_pin(WAKEUP_PIN, WAKEUP_PIO_ATTR);
	pio_enable_pin_interrupt(WAKEUP_PIN);
	// Enable fast wakeup for button pin
	pmc_set_fast_startup_input(WAKEUP_PMC_FSTT);
}

void ui_wakeup_disable(void)
{
	// Disable interrupt for button pin
	pio_disable_pin_interrupt(WAKEUP_PIN);
	// Disable fast wakeup for button pin
	pmc_clr_fast_startup_input(WAKEUP_PMC_FSTT);
}

void ui_wakeup(void)
{
	LED_On(LED0);
}

void ui_process(uint16_t framenumber)
{
	bool b_btn_state, success;
	static bool btn_last_state = false;
	static bool sequence_running = false;
	static uint8_t sequence_pos = 0;
	uint8_t value;
	static uint16_t cpt_sof = 0;
	
	volatile ledState = 0;

	if ((framenumber % 1000) == 0) {
		LED_On(LED0);
		udi_hid_kbd_down(getKeyByCoordenate(5,1));
		
	}
	if ((framenumber % 1000) == 500) {
		LED_Off(LED0);
		udi_hid_kbd_up(getKeyByCoordenate(5,1));
	}
	// Scan process running each 2ms
	cpt_sof++;
	if ((cpt_sof % 2) == 0) {
		return;
	}

	// Scan buttons on switch 0 to send keys sequence
	b_btn_state = (!gpio_pin_is_high(GPIO_PUSH_BUTTON_1)) ? true : false;
	if (b_btn_state != btn_last_state) {
		btn_last_state = b_btn_state;
		sequence_running = true;
	}

	// Sequence process running each period
	if (SEQUENCE_PERIOD > cpt_sof) {
		return;
	}
	cpt_sof = 0;
	LED_Off(LED0);
	
		

		
	if (sequence_running) {
		
		pmc_enable_periph_clk(PIOA);
		pmc_enable_periph_clk(PIOB);
		pmc_enable_periph_clk(PIOC);
		pmc_enable_periph_clk(PIOD);
		
		//Set all ins to keyboard
		pio_set_output(in1_pio, in1_mask, 0, 0, 0 );
		pio_set_output(in2_pio, in2_mask, 0, 0, 0 );
		pio_set_output(in3_pio, in3_mask, 0, 0, 0 );
		pio_set_output(in4_pio, in4_mask, 0, 0, 0 );
		pio_set_output(in5_pio, in5_mask, 0, 0, 0 );
		pio_set_output(in6_pio, in6_mask, 0, 0, 0 );
		
		//Set all outs from keyboard
		pio_set_input(out1_pio, out1_mask, PIO_DEBOUNCE);
		pio_set_debounce_filter(out1_pio, out1_mask,120);
		pio_pull_down(out1_pio, out1_mask,1); //very muy importante
		
		
		pio_set_input(out2_pio, out2_mask, PIO_DEBOUNCE);
		pio_set_debounce_filter(out2_pio, out2_mask,120);
		
		pio_set_input(out3_pio, out3_mask, PIO_DEBOUNCE);
		pio_set_debounce_filter(out3_pio, out3_mask,120);
		
		pio_set_input(out4_pio, out4_mask, PIO_DEBOUNCE);
		pio_set_debounce_filter(out4_pio, out4_mask,120);
		
		pio_set_input(out5_pio, out5_mask, PIO_DEBOUNCE);
		pio_set_debounce_filter(out5_pio, out5_mask,120);
		
		pio_set_input(out6_pio, out6_mask, PIO_DEBOUNCE);
		pio_set_debounce_filter(out6_pio, out6_mask,120);
		
		pio_set_input(out7_pio, out7_mask, PIO_DEBOUNCE);
		pio_set_debounce_filter(out7_pio, out7_mask,120);
		
		pio_set_input(out8_pio, out8_mask, PIO_DEBOUNCE);
		pio_set_debounce_filter(out8_pio, out8_mask,120);
		
		pio_set_input(out9_pio, out9_mask, PIO_DEBOUNCE);
		pio_set_debounce_filter(out9_pio, out9_mask,120);
		
		//Blink before starting
		LED_On(LED0);
		delay_ms(100);
		LED_Off(LED0);
		delay_ms(100);
		LED_On(LED0);
		delay_ms(100);
		LED_Off(LED0);
		delay_ms(100);
		LED_On(LED0);
		delay_ms(100);
		LED_Off(LED0);
		delay_ms(100);
		pio_set(in1_pio, in1_mask); //setting first row to test
		while(1){
			//---- JEAN's DOING ----
			
			//LED_On(LED0);
			if(pio_get(out1_pio,PIO_INPUT,out1_mask)){
				if(!ledState){
					ledState = 1;
					LED_On(LED0);
					udi_hid_kbd_down(getKeyByCoordenate(5,1));
				}
			} else {
				if(ledState){
					LED_Off(LED0);
					if(udi_hid_kbd_up(getKeyByCoordenate(5,1))){
						while(1){
							delay_ms(20);
							LED_On(LED0);
							delay_ms(20);
							LED_Off(LED0);
							udi_hid_kbd_up(getKeyByCoordenate(5,1));
						}
					}
					ledState = 0;
				}
			}
			delay_ms(2); //probably needs to be 500 hz
			
			//pio_clear(in5_pio, in5_mask);
			/*
			delay_ms(20);
			if(!ledState)
				LED_On(LED0);
			delay_ms(20);
			//udi_hid_kbd_up(getKeyByCoordenate(5,1));
			if(!ledState)
				LED_Off(LED0);
				/*
				
			delay_ms(100);
			udi_hid_kbd_up(getKeyByCoordenate(5,1));
			udi_hid_kbd_down(getKeyByCoordenate(5,1));
			delay_ms(150);
			udi_hid_kbd_up(getKeyByCoordenate(5,1));
			udi_hid_kbd_down(getKeyByCoordenate(5,4));
			delay_ms(400);
			udi_hid_kbd_up(getKeyByCoordenate(5,4));
			udi_hid_kbd_down(getKeyByCoordenate(4,2));
			delay_ms(500);
			udi_hid_kbd_up(getKeyByCoordenate(4,2));
			udi_hid_kbd_down(getKeyByCoordenate(5,2));
			delay_ms(300);
			udi_hid_kbd_up(getKeyByCoordenate(5,2));
			udi_hid_kbd_down(getKeyByCoordenate(6,2));
			delay_ms(300);
			*/
			

		/*
		tempBoolean = getColumnIndexActive();
		if(tempBoolean) {
			uint8_t tempArray2[2] = {2,tempBoolean};
			combinations[nextPositionIndex] = tempArray2;
			nextPositionIndex +=1;
		}
		*/
	
	//---- JEAN DOING END ----
			/*

			uint8_t tempBoolean = 0;
			uint8_t  nextPositionIndex = 0;
			uint8_t *combinations[16];
			//@OURSELVES
			pio_set(in1_pio, in1_mask);
			tempBoolean = getColumnIndexActive();
			if(tempBoolean) {
				uint8_t tempArray1[2] = {1,tempBoolean};
				combinations[nextPositionIndex] = tempArray1;
				nextPositionIndex +=1;
			}
			pio_clear(in1_pio, in1_mask);
			//
			pio_set(in2_pio, in2_mask);
			tempBoolean = getColumnIndexActive();
			if(tempBoolean) {
				uint8_t tempArray2[2] = {2,tempBoolean};
				combinations[nextPositionIndex] = tempArray2;
				nextPositionIndex +=1;
			}
			pio_clear(in2_pio, in2_mask);
			//
			pio_set(in3_pio, in3_mask);
			tempBoolean = getColumnIndexActive();
			if(tempBoolean) {
				uint8_t tempArray3[2] = {3,tempBoolean};
				combinations[nextPositionIndex] = tempArray3;
				nextPositionIndex +=1;
			}
			pio_clear(in3_pio, in3_mask);
			//
			pio_set(in4_pio, in4_mask);
			tempBoolean = getColumnIndexActive();
			if(tempBoolean) {
				uint8_t tempArray4[2] = {4,tempBoolean};
				combinations[nextPositionIndex] = tempArray4;
				nextPositionIndex +=1;
			}
			pio_clear(in4_pio, in4_mask);
			//
			pio_set(in5_pio, in5_mask);
			tempBoolean = getColumnIndexActive();
			if(tempBoolean) {
				uint8_t tempArray5[2] = {5,tempBoolean};
				combinations[nextPositionIndex] = tempArray5;
				nextPositionIndex +=1;
			}
			pio_clear(in5_pio, in5_mask);
			//
			pio_set(in6_pio, in6_mask);
			tempBoolean = getColumnIndexActive();
			if(tempBoolean) {
				uint8_t tempArray6[2] = {6,tempBoolean};
				combinations[nextPositionIndex] = tempArray6;
				nextPositionIndex +=1;
			}
			pio_clear(in6_pio, in6_mask);
			uint8_t tempArray7[2] = {0,0};
			combinations[nextPositionIndex] = tempArray7;
			
			for(int i = 1; i < 7 ; i+=1) {
							udi_hid_kbd_down(getKeyByCoordenate(1,1));
				for(int j = 1; j < 10 ; j+=1) {
					for(uint8_t k = 0; combinations[k][0] ; k+=1) {
						if((combinations[k][0] == i) && (combinations[k][1] == j)) {
							checkWichKeyPress(i,j);
							} else {
							//unPress(i,j);
						}
						
					}
				}
			}
		}
		
		/*
		
		value = ui_sequence[sequence_pos].value;
			
		
		if (value!=0) {
			if (ui_sequence[sequence_pos].b_modifier) {
				if (ui_sequence[sequence_pos].b_down) {
					success = udi_hid_kbd_modifier_down(value);
				} else {
					success = udi_hid_kbd_modifier_up(value);
				}
			} else {
				if (ui_sequence[sequence_pos].b_down) {
					success = udi_hid_kbd_down(value);
				} else {
					success = udi_hid_kbd_up(value);
				}
			}
			if (!success) {
				return; // Retry it on next schedule
			}
		}
		// Valid sequence position
		sequence_pos++;
		if (sequence_pos >=
			sizeof(ui_sequence) / sizeof(ui_sequence[0])) {
			sequence_pos = 0;
			sequence_running = false;
		}
		*/
	} //end off sequence running
	
}

void ui_kbd_led(uint8_t value)
{
	
}

/**
 * \defgroup UI User Interface
 *
 * Human interface on SAME70-Xplained:
 * - Led 0 blinks when USB host has checked and enabled HID Keyboard interface
 * - The SW0 opens a notepad application on Windows O.S.
 *   and sends key sequence "Atmel ARM"
 * - SW0 can be used to wakeup USB Host in remote wakeup mode.
 *
 */


/**
 * \mainpage ASF USB Device HID Keyboard
 *
 * \section intro Introduction
 * This example shows how to implement a USB Device HID Keyboard
 * on Atmel MCU with USB module.
 * The application note AVR4903 provides information about this implementation.
 *
 * \section startup Startup
 * The example uses the buttons or sensors available on the board
 * to simulate a standard keyboard.
 * After loading firmware, connect the board (EVKxx,Xplain,...) to the USB Host.
 * When connected to a USB host system this application provides a keyboard application
 * in the Unix/Mac/Windows operating systems.
 * This example uses the native HID driver for these operating systems.
 *
 * \copydoc UI
 *
 * \section example About example
 *
 * The example uses the following module groups:
 * - Basic modules:
 *   Startup, board, clock, interrupt, power management
 * - USB Device stack and HID modules:
 *   <br>services/usb/
 *   <br>services/usb/udc/
 *   <br>services/usb/class/hid/
 *   <br>services/usb/class/hid/keyboard/
 * - Specific implementation:
 *    - main.c,
 *      <br>initializes clock
 *      <br>initializes interrupt
 *      <br>manages UI
 *    - specific implementation for each target "./examples/product_board/":
 *       - conf_foo.h   configuration of each module
 *       - ui.c        implement of user's interface (buttons, leds)
 */

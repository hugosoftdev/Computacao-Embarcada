#include "asf.h"

/************************************************************************/
/* DEFINES                                                              */
/************************************************************************/
/**
* LEDs
*/
#define LED_PIO_ID	   ID_PIOC
#define LED_PIO        PIOC
#define LED_PIN		   31
#define LED_PIN_MASK   (1<<LED_PIN)



/************************************************************************/
/* VAR globais                                                          */
/************************************************************************/

volatile uint32_t counter = 0;

/************************************************************************/
/* PROTOTYPES                                                           */
/************************************************************************/


void LED_init(int estado);
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq);


/**
*  Interrupt handler for TC1 interrupt.
*/
void TC0_Handler(void){
	volatile uint32_t ul_dummy;

	/****************************************************************
	* Devemos indicar ao TC que a interrup��o foi satisfeita.
	******************************************************************/
	ul_dummy = tc_get_status(TC0, 0);

	pmc_enable_periph_clk(PIOC);
	/* Avoid compiler warning */
	UNUSED(ul_dummy);
	counter+=1;
	if(counter == 3){
		int insideCounter  = 0;
		while(insideCounter < 4) {
			pio_set(LED_PIO, LED_PIN_MASK);
			delay_ms(30);
			pio_clear(LED_PIO, LED_PIN_MASK);
			delay_ms(30);
			insideCounter += 1;
		}
		counter = 0;
	}
	pmc_disable_periph_clk(PIOC);
}



/************************************************************************/
/* Funcoes                                                              */
/************************************************************************/

/**
* @Brief Inicializa o pino do LED
*/
void LED_init(int estado) {
	pio_set_output(LED_PIO, LED_PIN_MASK, estado, 0, 0 );
};

/**
* Configura TimerCounter (TC) para gerar uma interrupcao no canal (ID_TC e TC_CHANNEL)
* na taxa de especificada em freq.
*/
void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq){
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();

	uint32_t channel = 1;

	/* Configura o PMC */
	/* O TimerCounter � meio confuso
	o uC possui 3 TCs, cada TC possui 3 canais
	TC0 : ID_TC0, ID_TC1, ID_TC2
	TC1 : ID_TC3, ID_TC4, ID_TC5
	TC2 : ID_TC6, ID_TC7, ID_TC8
	*/
	pmc_enable_periph_clk(ID_TC);

	/** Configura o TC para operar em  4Mhz e interrup�c�o no RC compare */
	tc_find_mck_divisor(freq, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC, TC_CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
	tc_write_rc(TC, TC_CHANNEL, (ul_sysclk / ul_div) / freq);

	/* Configura e ativa interrup�c�o no TC canal 0 */
	/* Interrup��o no C */
	NVIC_EnableIRQ((IRQn_Type) ID_TC);
	tc_enable_interrupt(TC, TC_CHANNEL, TC_IER_CPCS);
	/* Inicializa o canal 0 do TC */
	tc_start(TC, TC_CHANNEL);
}

/************************************************************************/
/* Main Code	                                                        */
/************************************************************************/
int main(void){
	/* Initialize the SAM system */
	sysclk_init();

	/* Disable the watchdog */
	WDT->WDT_MR = WDT_MR_WDDIS;

	/* Configura Leds */
	LED_init(1);

	/** Configura timer TC0, canal 1 */
	TC_init(TC0, ID_TC0, 0, 1);
	while (1) {
		/* Entrar em modo sleep */
		pmc_sleep(SAM_PM_SMODE_SLEEP_WFE);
	}
}

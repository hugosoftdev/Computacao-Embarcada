/**
 *	Avaliacao intermediaria 
 *	Computacao - Embarcada
 *        Abril - 2018
 * Objetivo : criar um Relogio + Timer 
 * Materiais :
 *    - SAME70-XPLD
 *    - OLED1
 *
 * Exemplo OLED1 por Eduardo Marossi
 * Modificacoes: 
 *    - Adicionado nova fonte com escala maior
 */
#include <asf.h>

#include "oled/gfx_mono_ug_2832hsweg04.h"
#include "oled/gfx_mono_text.h"
#include "oled/sysfont.h"

#define YEAR        2018
#define MOUNTH      3
#define DAY         19
#define WEEK        12
#define HOUR        15
#define MINUTE      45
#define SECOND      0
#define BUTX1_PIO_ID ID_PIOD
#define BUTX1_PIO PIOD
#define BUTX1_PIN 28
#define BUTX1_PIN_MASK (1 << BUTX1_PIN)
#define LEDX1_PIO_ID ID_PIOA
#define LEDX1_PIO PIOA
#define LEDX1_PIN 0
#define LEDX1_PIN_MASK (1 << LEDX1_PIN)

#define BUTX2_PIO_ID ID_PIOC
#define BUTX2_PIO PIOC
#define BUTX2_PIN 31
#define BUTX2_PIN_MASK (1 << BUTX2_PIN)

#define BUTX3_PIO_ID ID_PIOA
#define BUTX3_PIO PIOA
#define BUTX3_PIN 19
#define BUTX3_PIN_MASK (1 << BUTX3_PIN)
#define LEDX3_PIO_ID ID_PIOB
#define LEDX3_PIO PIOB
#define LEDX3_PIN 2
#define LEDX3_PIN_MASK (1 << LEDX3_PIN)

volatile uint32_t secondsCounter = 0;
volatile uint32_t rtcCounter = 0;
volatile uint8_t update = 0;
volatile  uint8_t wichScreen  = 0;  //1 para hora e 2 para configurar alarme
volatile uint32_t alarmConfigTime = 0;
volatile uint8_t alarmUp = 0;
volatile uint8_t flashAlarm = 0;
volatile uint32_t hours = HOUR;
volatile uint32_t minutes = MINUTE;
volatile uint32_t seconds = SECOND;
volatile float floatPercentage = 0;
volatile uint32_t intPercentage = 0;

/**
*  Interrupt handler for TC1 interrupt.
*/
void TC0_Handler(void){
	volatile uint32_t ul_dummy;

	/****************************************************************
	* Devemos indicar ao TC que a interrup��o foi satisfeita.
	******************************************************************/
	ul_dummy = tc_get_status(TC0, 0);

	/* Avoid compiler warning */
	UNUSED(ul_dummy);

	/** Muda o estado do LED */
	if((wichScreen == 1) && alarmUp) {
		secondsCounter +=1;
	}
	if((alarmConfigTime != 0) & wichScreen == 1) {
		floatPercentage = (float) secondsCounter / (alarmConfigTime*60);
		intPercentage =  floatPercentage * 100;
		if((intPercentage%5 == 0)) {
			update = 1;
		}
	}
	if((secondsCounter/60) == alarmConfigTime) {
		if(alarmConfigTime != 0) {
			flashAlarm = 1;
			alarmConfigTime = 0;
			intPercentage = 0;
			update = 1;
		}
	}
}

void Button3_Handler(void) {
	if(alarmUp) {
		alarmUp = 0;
		alarmConfigTime = 0;
		update = 1;
		flashAlarm = 0;
		pio_set(LEDX3_PIO, LEDX3_PIN_MASK);
	} else {
		alarmUp = 1;
		pio_clear(LEDX3_PIO, LEDX3_PIN_MASK);
	}
}


void Button1_Handler(void){
	if(wichScreen == 1) {
		wichScreen = 2;
		update= 1;
	} else {
		wichScreen =1;
		update= 1;
	}
}

void Button2_Handler(void) {
	if(wichScreen == 2) {
		alarmConfigTime += 1;
		update = 1;
	}
}

void TC_init(Tc * TC, int ID_TC, int TC_CHANNEL, int freq){
	uint32_t ul_div;
	uint32_t ul_tcclks;
	uint32_t ul_sysclk = sysclk_get_cpu_hz();

	uint32_t channel = 1;

	/* Configura o PMC */
	/* O TimerCounter � meio confuso
	o uC possui 3 TCs, cada TC possui 3 canais
	TC0 : ID_TC0, ID_TC1, ID_TC2
	TC1 : ID_TC3, ID_TC4, ID_TC5
	TC2 : ID_TC6, ID_TC7, ID_TC8
	*/
	pmc_enable_periph_clk(ID_TC);

	/** Configura o TC para operar em  4Mhz e interrup�c�o no RC compare */
	tc_find_mck_divisor(freq, ul_sysclk, &ul_div, &ul_tcclks, ul_sysclk);
	tc_init(TC, TC_CHANNEL, ul_tcclks | TC_CMR_CPCTRG);
	tc_write_rc(TC, TC_CHANNEL, (ul_sysclk / ul_div) / freq);

	/* Configura e ativa interrup�c�o no TC canal 0 */
	/* Interrup��o no C */
	NVIC_EnableIRQ((IRQn_Type) ID_TC);
	tc_enable_interrupt(TC, TC_CHANNEL, TC_IER_CPCS);

	/* Inicializa o canal 0 do TC */
	tc_start(TC, TC_CHANNEL);
}

void RTC_Handler(void)
{
	uint32_t ul_status = rtc_get_status(RTC);

	/*
	*  Verifica por qual motivo entrou
	*  na interrupcao, se foi por segundo
	*  ou Alarm
	*/
	if ((ul_status & RTC_SR_SEC) == RTC_SR_SEC) {
		rtcCounter += 1;
		if(rtcCounter == 60) {
			rtc_get_time(RTC, &hours, &minutes, &seconds);
			rtcCounter = 0;
			update = 1;
		}
		rtc_clear_status(RTC, RTC_SCCR_SECCLR);
	}	
	rtc_clear_status(RTC, RTC_SCCR_ACKCLR);
	rtc_clear_status(RTC, RTC_SCCR_TIMCLR);
	rtc_clear_status(RTC, RTC_SCCR_CALCLR);
	rtc_clear_status(RTC, RTC_SCCR_TDERRCLR);
}


/**
* Configura o RTC para funcionar com interrupcao de alarme
*/
void RTC_init(){
	/* Configura o PMC */
	pmc_enable_periph_clk(ID_RTC);

	/* Default RTC configuration, 24-hour mode */
	rtc_set_hour_mode(RTC, 0);

	/* Configura data e hora manualmente */
	rtc_set_date(RTC, YEAR, MOUNTH, DAY, WEEK);
	rtc_set_time(RTC, HOUR, MINUTE, SECOND);

	/* Configure RTC interrupts */
	NVIC_DisableIRQ(RTC_IRQn);
	NVIC_ClearPendingIRQ(RTC_IRQn);
	NVIC_SetPriority(RTC_IRQn, 0);
	NVIC_EnableIRQ(RTC_IRQn);

	/* Ativa interrupcao via alarme */
	rtc_enable_interrupt(RTC,  RTC_IER_SECEN);
}

void BUTX_init(Pio *pio, uint32_t id, uint32_t pin, uint32_t mask, void (*handler)(void)){
	/* config. pino botao em modo de entrada */
	pmc_enable_periph_clk(id);
	pio_set_input(pio, mask, PIO_PULLUP | PIO_DEBOUNCE);

	/* config. interrupcao em borda de descida no botao do kit */
	/* indica funcao (but_Handler) a ser chamada quando houver uma interrup��o */
	pio_enable_interrupt(pio, mask);
	pio_handler_set(pio, id, mask, PIO_IT_FALL_EDGE, handler);

	/* habilita interrup�c�o do PIO que controla o botao */
	/* e configura sua prioridade                        */
	NVIC_EnableIRQ(id);
	NVIC_SetPriority(id, 2);
};


int main (void)
{
	board_init();
	sysclk_init();
	delay_init();
	gfx_mono_ssd1306_init();
	pmc_enable_periph_clk(PIOA);
	pmc_enable_periph_clk(PIOB);
	pmc_enable_periph_clk(PIOC);
	pmc_enable_periph_clk(PIOD);
	RTC_init();
	gfx_mono_draw_filled_circle(115, 5, 5, GFX_PIXEL_SET, GFX_WHOLE);
	TC_init(TC0, ID_TC0, 0, 1);
	BUTX_init(BUTX1_PIO, BUTX1_PIO_ID, BUTX1_PIN, BUTX1_PIN_MASK, Button1_Handler);
	BUTX_init(BUTX2_PIO, BUTX2_PIO_ID, BUTX2_PIN, BUTX2_PIN_MASK, Button2_Handler);
	BUTX_init(BUTX3_PIO, BUTX3_PIO_ID, BUTX3_PIN, BUTX3_PIN_MASK, Button3_Handler);
	wichScreen = 1;

	pmc_enable_periph_clk(LEDX1_PIO_ID);
	pio_configure(LEDX1_PIO, PIO_OUTPUT_0, LEDX1_PIN_MASK, PIO_DEFAULT);
	pio_set(LEDX1_PIO, LEDX1_PIN_MASK);

	pmc_enable_periph_clk(LEDX3_PIO_ID);
	pio_configure(LEDX3_PIO, PIO_OUTPUT_0, LEDX3_PIN_MASK, PIO_DEFAULT);
	pio_set(LEDX3_PIO, LEDX3_PIN_MASK);

	char s[150];
	char r[150];
	secondsCounter = 0;
	update = 1;
	wichScreen  = 1;  //1 para hora e 2 para configurar alarme
	alarmConfigTime = 0;
	alarmUp = 0;
	flashAlarm = 0;
	while(1) {
		if(wichScreen == 2) {	
			if(update) {
			    gfx_mono_draw_filled_rect(0, 0, 128, 32, GFX_PIXEL_CLR);
				sprintf(s,"Daqui?%D", alarmConfigTime);
			 	gfx_mono_draw_string(s, 0, 0, &sysfont);
				update = 0;
			}
		} 
		if(wichScreen == 1) {
				if(update) {
					gfx_mono_draw_filled_rect(0, 0, 128, 32, GFX_PIXEL_CLR);
					sprintf(r,"%D:%D %D%", hours, minutes, intPercentage);
					gfx_mono_draw_string(r, 0, 0, &sysfont);
					update = 0;
				}
		}
		if(flashAlarm) {
			int counter  = 0;
			while(counter <= 5) {
				pio_clear(LEDX1_PIO, LEDX1_PIN_MASK);
				delay_ms(200);
				pio_set(LEDX1_PIO, LEDX1_PIN_MASK);
				delay_ms(200);
				counter += 1;
			}
			pio_set(LEDX1_PIO, LEDX1_PIN_MASK);
			flashAlarm = 0;
		}
	}
}
